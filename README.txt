
views_mail

OVERVIEW

The Views Mail module provides a flexible method for sending messages to
list of users created by the Views module.  

This module was originally created by KarenS and modified by somebodysysop.
Details here: http://drupal.org/node/134931

This tool is essentially a smart query builder for building e-mail
lists. 

FEATURES

-Create recipient list using Views to send mass email.
-Test mode available.
-Installs Views filters for filtering nodes by OG Group and roles.
-Can schedule emails for future delivery.
-Uses messages created as HTML newsletters in Simplenews module.
-Places group e-mail opt-out option in user's account.
-Option to force e-mail opt-out.
-Option to auto-subscribe all new signups to a newsletter.
-Option to auto-subscribe all new group members to a newsletter.

REQUIREMENTS

Views Mail requires the following modules to already be installed:

-Views (to create the email lists) http://www.drupal.org/project/views
-CCK (for userreference and nodereference) http://www.drupal.org/project/cck 
-Simplenews (to create the email messages)  http://drupal.org/project/simplenews
-Mimemail (to send emails in HTML format) http://drupal.org/project/mimemail
-Actions* (to schedule future delivery of emails) http://drupal.org/project/actions
-Scheduled Actions (to schedule future delivery of emails) 
 http://drupal.org/project/sched_act
 No 5.x version available yet, so, you'll have to patch the 4.7 version.
 Details here: http://drupal.org/node/122391

*You need actions module for the userreference and nodereference modules it supplies.
 These are necessary to automatically get the email address of the author of a node.

INSTALLING VIEWS MAIL

Make sure you have installed the required modules above and configured them.

Gunzip and untar the Views Mail tarball and place it into your modules  
folder. Navigate to Admin->Modules and click on Views Mail to intstall.

Simplenws Settings
------------------
Admin-> Content Managment -> Newsletters -> Newsletters Tab -> Add Newsletter Link

Add a Newsletter type that you will use for creating newsletter issues that
will be used with Views Mail.

Views Mail Settings
-------------------
Admin -> Site Configuration - > Views Mail Configuration

Default Newsletter:	

	Select the default newsletter type which will contain the
	newsletter issues you will send with Views Mail.  This
	default newsletter type should have been created in the
	Simplenews Settings step above.

Force group e-mail opt-in:	

	When new user signs up, force the group e-mail 
	opt-in to be checked ON.  This means the option 
	will NOT appear on the user's registration
	page but will appear in the user's "My Account"
	screen.  User can uncheck the option after
	registration is completed.

Subscribe all new signups to newsletter:	

	Enter here a newsletter id if 
	you want all new signups to be 
	subscribed to this newsletter.

Groups whose new users are autosubscribed to newsletters:

	Enter here a group id | newsletter id pair
	if you want all new members to this group
	to be subscribed to this newsletter.

Test Settings

	"Email test mode" -	Click this on if you want to run 
	in test mode (i.e., no actual emails sent).

USING VIEWS_MAIL

1. Create the message you wish to send using Simplenews.  

   Create Content -> Newsletter.
   
   Save it as the newsletter type you selected for Views Mail.
   
2. Go to views and create a view of the users you wish to send the message to.  You can 
   add exposed filters if you wish end-users to be able to customize the view.
    
   Admin -> Views -> Add View.  Don't forget to:
   
   a) Select "Views mail" as the Views Type.
   b) Select at least one field that contains the e-mail address you wish the message
      to. Otherwise, the message will be sent to the e-mail address of the node
      author.
   c) Save the view.

3. Go to the view you created in Step 2. Click on the "Send Mail" link to open the
   Views Mail window.  You will see these options for sending your email:

		Recipient name	:	Select the View field to use for the recipient's name.
		Recipient email	:	Select the View field to use for the recipient's email address.
		Subject			:	Enter the text to be used as the email subject line.
		Newsletter to 
		  send to this 
		  group			: 	Select the Simplenews newsletter you created in Step 1 that
							contains the message you want sent.

		Distinct		:	Select "Yes" here. Not sure what this does.

		If wish to schedule delivery of this email for a future date, you 
		can click on the "Schedule" link:

		Number			:	The number of time units below to wait before sending this
							email. Enter a number.
		Unit			:	Unit of time for the number entered above.  Seconds, Minutes,
							Hours or Days.

4. Once you have completed entering the "Send Mail" options in the Views Mail window, click 
   on the "Send" button at the bottom of this window.  You will then be given a summary
   view of all the elments you selected in Step 3 for verification.  If all is correct,
   then click on "Confirm" to send/schedule the email.  If not, go back and correct.


DOCUMENTATION

There currently is no documentation other then this README.txt file and the original
discussions we had on creation of the module here: http://drupal.org/node/134931

-------------- IMPORTANT --------------------------------------------------
-------------- IMPORTANT --------------------------------------------------

If you update Views or any module that uses Views, you MUST MUST MUST
go and resubmit the admin/modules page. Views caches information provided
by modules, and this information MUST be re-cached whenever an update
is performed. 

DRUPAL CANNOT TELL AUTOMATICALLY IF YOU HAVE UPDATED CODE. Therefore you
must go and submit this page.

-------------- TODO ---------------------------------------------

- set up userreference and nodereference fields to use the name and email 
  of the userreference and nodereference uid
- incorporate Token module to handle substitutions


CREDITS

Huge kudos to KarenS for creating this module!
